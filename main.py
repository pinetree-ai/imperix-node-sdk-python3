# Developed by Aptus Engineering, Inc. <https://aptus.aero>
# See LICENSE.md file in project root directory

import sys
import asyncio
import numpy as np
from PIL import Image
from datetime import datetime

from imperix import NodeLink


# Set up callback functions
# Called when mission is updated
async def missionUpdateCallback(mission):
    print(mission)


# Called with manual control command is received
async def manualControlCallback(control):
    print(control)


# Instantiate node link with callbacks
node = NodeLink(
    missionUpdateCallback=missionUpdateCallback,
    manualControlCallback=manualControlCallback
)

async def main():
    
    configFile = sys.argv[1] if len(sys.argv) > 1 else 'node.cfg'

    # Connect and authenticate
    await node.connect(config=configFile)

    # await node.streamVideoFromSource("PRIMARY", "/dev/video0")
    # await node.streamVideoFromSource("RPICAM", "/dev/video1")

    # Main loop...
    while True:
        
        await node.transmitTelemetry({
            "TIMESTAMP": str(datetime.utcnow()),
            "ATT_PITCH": 0,
            "ATT_ROLL": 0,
            "ATT_HEADING": 0,
            "LOC_LATITUDE": 0,
            "LOC_LONGITUDE": 0,
            "LOC_ALTITUDE": 0,
            "VEL_GROUNDSPEED": 0,
            "VEL_AIRSPEED": 0,
            "VEL_VERTICAL_SPEED": 0,
            "STS_BATTERY": 50,
            "STS_SIGNAL": 75
        })

        await asyncio.sleep(0.02)
        


# Start event loop
eventLoop = asyncio.get_event_loop()

try:
    asyncio.ensure_future(main())               # Main thread
    eventLoop.run_forever()

except KeyboardInterrupt:
    pass

finally:
    eventLoop.close()